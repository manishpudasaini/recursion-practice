package Recursion;

import java.util.Scanner;
//find the nth term of fibonacci series
public class FabonacciTerm {
    public static void display(int start,int end,int array[]){
        if(start==end){
            return;
        }
        array[start]=array[start-2]+array[start-1];
        System.out.println(array[start]);
        display(start+1,end,array);

    }

    public static void main(String[] args) {

        Scanner sc =new Scanner(System.in);
        System.out.println("Enter the index you want to find::");
        int size=sc.nextInt();

        int [] array = new int[size];
        array[0]=0;
        array[1]=1;

        display(2,size,array);

    }
}
